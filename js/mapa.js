var mapa = '';

$(document).ready(function() {
    // create a map in the "map" div
    mapa = L.map('mapa').setView([-34.6168, -58.5056], 12);

    // add an OpenStreetMap tile layer
    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(mapa);
});

function mostrarPuntoMapa (latitud, longitud, descripcion) {
    // ubica el mapa en el punto indicado. Zoom: 13
    mapa.setView([latitud, longitud], 16);

    // add a marker in the given location, attach some popup content to it and open the popup
    L.marker([latitud, longitud]).addTo(mapa)
        .bindPopup(descripcion)
        .openPopup();

    var element = document.getElementsByClassName('mapa-container');
        element[0].scrollIntoView(false);
};