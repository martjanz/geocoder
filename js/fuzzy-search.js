$( document ).ready(function() {

	//Jurisdicciones del área de estudio
	var jurisdicciones = [];
	//Puede contener calles o hitos, según el valor ingresado en el campo anterior
	var callesHitos = [];

	var jurisdiccion = '';
	var calleA = '';
	var calleB = '';

	// Tipo de item en primer typeahead (calle o hito)
	var tipoItemA = '';

	//Nombres de calles que intersectan con la primera calle ingresada
	var intersecciones = [];

    //Rango de alturas de calle válidas
    var alturasCalle;

	jurisdicciones = cargarJurisdicciones();


	/* Typeahead para input de jurisdicción */
	$('input.jurisdiccion').typeahead({
		matcher: function (item) {
			return fuzzyMatcher(item, this.query, 3, false);
		},
		highlighter: function (item) {
			return fuzzyHighlighter(item, this.query, false, 3, false);
		},
		source: jurisdicciones,
		updater: function (item) {
			callesA = cargarCalles(item);

			jurisdiccion = item;

			console.log("Jurisdicción: ", jurisdiccion);

			//Devuelve el string que va al input (e.g. name)
			return item;
		}
	});


	/* Typeahead para input de calle */
	$('input.fuzzy-calle-hito').typeahead( {
		matcher: function (item) {
			return fuzzyMatcher(item, this.query, 3, false);
		},
		highlighter: function (item) {
			return fuzzyHighlighter(item, this.query, false, 3, false);
		},
		source: function (item) {
			return callesHitos;
		},
		items: 15,
		updater: function (item) {
			if (tipoItemA === 'calle') {
				calleA = item;
				console.log("Calle A", calleA);
			} else {
				hito = item;
				console.log("Hito", hito)

				coordenadas = getCoordenadasHito(jurisdiccion, hito);

				console.log(coordenadas.toString());
				console.log("Lat: " + coordenadas.lat);
				console.log("Lon: " + coordenadas.lon);

				$('#latitud').text("Latitud: " + coordenadas.lat);
				$('#longitud').text("Longitud: " + coordenadas.lon);

				$('#google-maps-link').html("<a href='https://www.google.com/maps/@"+coordenadas.lat+","+coordenadas.lon+",18z' target='_blank'>Ver en Google Maps</a>");
				$('#osm-link').html("<a href='http://www.openstreetmap.org/#map=18/"+coordenadas.lat+"/"+coordenadas.lon+"' target='_blank'>Ver en OpenStreetMap</a>");

				mostrarPuntoMapa(coordenadas.lat, coordenadas.lon, '<strong>Hito:</strong> ' + hito);
			}

			//Devuelve el string que va al input (e.g. name)
			return item;
		}
	});


	/* Typeahead de calles intersecantes con la primera calle  */
	$(':input.fuzzy-altura-esquina').typeahead( {
	    matcher: function (item) {
	        return fuzzyMatcher(item, this.query, 3, false);
	    },
	    highlighter: function (item) {
	        return fuzzyHighlighter(item, this.query, false, 3, false);
	    },
	    source: function (item) {
	    	//return the display array
	      return intersecciones;
	    },
	    items: 15,
	    updater: function (item) {
	    	calleB = item;

			var coordenadas = new getCoordenadasDireccion(jurisdiccion, calleA, calleB);

			console.log(coordenadas.toString());
			console.log("Lat: " + coordenadas.lat);
			console.log("Lon: " + coordenadas.lon);

			$('#latitud').text("Latitud: " + coordenadas.lat);
			$('#longitud').text("Longitud: " + coordenadas.lon);

			$('#google-maps-link').html("<a href='https://www.google.com/maps/@"+coordenadas.lat+","+coordenadas.lon+",18z' target='_blank'>Ver en Google Maps</a>");
			$('#osm-link').html("<a href='http://www.openstreetmap.org/#map=18/"+coordenadas.lat+"/"+coordenadas.lon+"' target='_blank'>Ver en OpenStreetMap</a>");

			mostrarPuntoMapa(coordenadas.lat, coordenadas.lon, '<strong>Esquina</strong><br>' + calleA + ' y ' + calleB);

			//Devuelve el string que va al input (e.g. name)
			return item;
    }
	});


	function getCalles(orden) {
		if (orden == 'A') {
			return callesA;
		} else {
			return callesB;
		}
	};


	/**
	 * Sólo permite ingresar "C" o "H".
	 *
	 * Original: http://stackoverflow.com/questions/891696/jquery-what-is-the-best-way-to-restrict-number-only-input-for-textboxes-all
	 */
	$( 'input.selector-calle-hito' ).keypress(function(event) {
		// Teclas no numéricas (de control) aceptadas. Exceptúa el 39 (' en Firefox)

		// En orden: Backspace, tab, enter, fin, inicio, izquierda, derecha
		var teclasControl = [0, 8, 9, 13, 35, 36, 37];

		// indexOf no está soportado en Internet Explorer
		var esTeclaControl = teclasControl.join(",").match(new RegExp(event.which));

		//Convierto valor a mayúscula
		var valor = String.fromCharCode(event.charCode).toUpperCase();

		var esCaracterValido = valor.match(new RegExp('C|H'));

		// Algunos navegadores no lanzan eventos para las teclas de control. Por ej.: backspace en Safari.
		if (!event.which || // Teclas de control en la mayoría de los navegadores. Por ej.: en Firefox, tab es 0.
			esCaracterValido || // De 0 a 9
			esTeclaControl) { // Opera (entre otros) asigna valores a teclas de control.

			if ( esCaracterValido ) {
				this.value = valor;

				event.preventDefault();
			}

		} else {
			event.preventDefault();
		}
	});


	/* La solicitud de array de calles o hitos ocurre cuando el foco sale del input
	y el valor contenido es válido. */
	$( 'input.selector-calle-hito' ).change(function(event) {
		if (this.value === 'C') {
			//Cargar array de calles
			console.log('Carga array de calles. Jurisdicción:', jurisdiccion);

			tipoItemA = 'calle';

			callesHitos = cargarCalles(jurisdiccion);

		} else if (this.value === 'H') {
			//Carga array de hitos
			console.log('Carga array de hitos. Jurisdicción:', jurisdiccion);

			tipoItemA = 'hito';

			callesHitos = cargarHitos(jurisdiccion);
		}
	});


	/* La solicitud de array de calles o hitos ocurre cuando el foco sale del input
	y el valor contenido es válido. */
	$( 'input.selector-altura-esquina' ).change(function(event) {
		if (this.value === 'A') {
			$('input.fuzzy-altura-esquina')
                .addClass('altura-calle')
                .addClass('datatype-integer')
                .removeClass('typeahead');

            //Cargar array de calles
            console.log('Carga alturas. Jurisdicción: ' + jurisdiccion + ', Calle: ' + calleA);

            alturasCalle = cargarAlturas(jurisdiccion, calleA);

		} else if (this.value === 'E') {
			$('input.fuzzy-altura-esquina')
                .removeClass('altura-calle')
                .removeClass('datatype-integer')
                .addClass('typeahead');

			console.log('Carga intersecciones. Jurisdicción: ' + jurisdiccion + ', Calle: ' + calleA);

            //Cargar array de intersecciones
			intersecciones = cargarIntersecciones(jurisdiccion, calleA);
		}
	});


    /* Inputs altura de calle */
    $( 'body').on('blur', 'input.altura-calle', function(event) {
        var valorValido = new Boolean;

        //Limpia estado de error previo
        $(this).parent().find('p').remove();
        $(this).parent().removeClass('has-error');

        if ( isNaN(Number(this.value)) ) {
            valorValido = false;
        } else {
            if ( Number(this.value) < alturasCalle[0] ||
                    Number(this.value) > alturasCalle[1]) {
                valorValido = false;
            } else {
                valorValido = true;
            };
        }

        if ( valorValido ) {
            $(this).parent().addClass('has-success');
        } else {
            $(this).parent().addClass('has-error');
            $(this).parent()
                .append('<p class="help-block">Altura fuera de rango.</p>');
        }
    });

});

function cargarJurisdicciones() {
	//the "process" argument is a callback, expecting an array of values (strings) to display

	//reset these containers
	var nombres = [];

	queryUrl = "api/sql.php?table=intersecciones_vw&fields=DISTINCT jurisdiccion as nombre";
	//get the data to populate the typeahead (plus some)
	//from your api, wherever that may be
	$.getJSON( queryUrl, function ( data ) {
		//for each item returned, if the display name is already included
		//(e.g. multiple "John Smith" records) then add a unique value to the end
		//so that the user can tell them apart. Using underscore.js for a functional approach.
		_.each( data, function( item, ix, list ){
			//add the label to the display array
			nombres.push( item.nombre );
		});
	});

	//return the display array
	return nombres;
};

function cargarCalles(jurisdiccion, interseccion) {
	//reset these containers
	var nombres = [];

	if ( !interseccion ) {
		queryUrl = "api/sql.php?table=intersecciones_vw&fields=DISTINCT calle_a as nombre&parameters=jurisdiccion='" + jurisdiccion.toUpperCase() + "'";
	} else {
		queryUrl = "api/sql.php?table=intersecciones_vw&fields=DISTINCT calle_b as nombre&parameters=jurisdiccion='" + jurisdiccion.toUpperCase() + "' and calle_a = '" + interseccion + "'";
	}

	//get the data to populate the typeahead (plus some)
	//from your api, wherever that may be
	$.getJSON(queryUrl, function ( data ) {
		//for each item returned, if the display name is already included
		//(e.g. multiple "John Smith" records) then add a unique value to the end
		//so that the user can tell them apart. Using underscore.js for a functional approach.
		_.each( data, function( item, ix, list ){
			//add the label to the display array
			nombres.push( item.nombre );
		});
	});

	//return the display array
	return nombres;
};


function cargarHitos(jurisdiccion) {
	//reset these containers
	var nombres = [];

	queryUrl = "api/sql.php?table=hitos_vw&fields=nombre&parameters=jurisdiccion = '" + jurisdiccion.toUpperCase() + "'";

	//get the data to populate the typeahead (plus some)
	//from your api, wherever that may be
	$.getJSON(queryUrl, function ( data ) {
		//for each item returned, if the display name is already included
		//(e.g. multiple "John Smith" records) then add a unique value to the end
		//so that the user can tell them apart. Using underscore.js for a functional approach.
		_.each( data, function( item, ix, list ){
			//add the label to the display array
			nombres.push( item.nombre );
		});
	});

	//return the display array
	return nombres;
};


function cargarIntersecciones(jurisdiccion, calle) {
  var nombres = [];

  queryUrl = "api/sql.php?table=intersecciones_vw&fields=DISTINCT calle_b as nombre&parameters=jurisdiccion = '" + jurisdiccion.toUpperCase() + "' AND calle_a = '" + calle.toUpperCase() + "'";

  //get the data to populate the typeahead (plus some)
  //from your api, wherever that may be
  $.getJSON(queryUrl, function ( data ) {
    //for each item returned, if the display name is already included
    //(e.g. multiple "John Smith" records) then add a unique value to the end
    //so that the user can tell them apart. Using underscore.js for a functional approach.
    _.each( data, function( item, ix, list ){
      //add the label to the display array
      nombres.push( item.nombre );
    });
  });

  //Devuelve array de hitos
  return nombres;
};


function cargarAlturas(jurisdiccion, calle) {
    var alturas = [];

    queryUrl = "api/sql.php?table=alturas_vw&fields=altura_desde as desde, altura_hasta as hasta&parameters=nombre_jurisdiccion = '" + jurisdiccion.toUpperCase() + "' AND nombre_calle = '" + calle.toUpperCase() + "' LIMIT 1";

    $.getJSON(queryUrl, function ( data ) {
        //for each item returned, if the display name is already included
        //(e.g. multiple "John Smith" records) then add a unique value to the end
        //so that the user can tell them apart. Using underscore.js for a functional approach.
        _.each( data, function( item, ix, list ){
          //add the label to the display array
          alturas.push( item.desde, item.hasta);
        });
      });

    //Devuelve alturas
    return alturas;
};


/*
function getCoordenadas()
	Parámetros
		jurisdiccion: nombre de jurisdicción
		calleA: nombre de calle que intersecta con calleB
		calleB: nombre de calle que intersecta con calleA

	Devuelve:
		un objeto con latitud (lat) y longitud (lon)
*/
function getCoordenadasDireccion(jurisdiccion, calleA, calleB) {
	queryUrl = "api/sql.php?table=intersecciones_vw&fields=ST_X(ST_Transform(geom,4326)) lon, ST_Y(ST_Transform(geom,4326)) lat&parameters=jurisdiccion = '" + jurisdiccion.toUpperCase() + "' and calle_a = '"+ calleA.toUpperCase() +"' AND calle_b = '" + calleB.toUpperCase() + "'&limit=1";

	var returnObj = this;
	var data = '';

	/* Uso $.ajax y no $.getJSON porque necesito que el request sea sincrónico. */
	$.ajax({
		url: queryUrl,
		dataType: 'json',
		async: false,
		data: data,
		success: function ( data ) {
			console.log(data);
			returnObj.lat = data[0].lat;
			returnObj.lon = data[0].lon;
		}
	});

	console.log("this.lat: " + this.lat);

	return this;
}


/*
function getCoordenadas()
	Parámetros
		jurisdiccion: nombre de jurisdicción
		calleA: nombre de calle que intersecta con calleB
		calleB: nombre de calle que intersecta con calleA

	Devuelve:
		un objeto con latitud (lat) y longitud (lon)
*/
function getCoordenadasHito(jurisdiccion, hito) {
	queryUrl = "api/sql.php?table=hitos_vw&fields=ST_X(ST_Transform(geom,4326)) lon, ST_Y(ST_Transform(geom,4326)) lat&parameters=jurisdiccion = '" + jurisdiccion.toUpperCase() + "' AND nombre = '" + hito.toUpperCase() + "'";

	var returnObj = this;
	var data = '';

	/* Uso $.ajax y no $.getJSON porque necesito que el request sea sincrónico. */
	$.ajax({
		url: queryUrl,
		dataType: 'json',
		async: false,
		data: data,
		success: function ( data ) {
			console.log(data);
			returnObj.lat = data[0].lat;
			returnObj.lon = data[0].lon;
		}
	});

	console.log("this.lat: " + this.lat);

	return this;
}