$(document).ready(function() {
    //En foco de "nombre de calle/hito" limpia opciones de campos siguientes.
    $('.selector-calle-hito').focus(function() {
        //Limpia nombre de calle/hito
        $('#input-item-a').val("");

        //Limpia selección de radiobutton "Esquina"
        $('#rd-esquina').prop('checked', false);

        //Limpia input calle-altura
        $('#input-item-b').val("");
    });

    //En foco de "nombre de calle/hito" limpia opciones de campos siguientes.
    $('#input-item-a').focus(function() {
        //Limpia selección de radiobutton "Esquina"
        $('#rd-esquina').prop('checked', false);

        //Limpia input calle-altura
        $('#input-item-b').val("");
    });
});