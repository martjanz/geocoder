/* Reemplaza caracteres especiales por su equivalente más simple. */
function cleanUpSpecialChars(str)
{
    str = str.replace(/[ÀÁÂÃÄÅ]/g,"A");
    str = str.replace(/[àáâãäå]/g,"a");
    str = str.replace(/[ÈÉÊË]/g,"E");
    str = str.replace(/[ÈÉÊË]/g,"e");
    str = str.replace(/[ÍÌÎÏ]/g,"I");
    str = str.replace(/[íìîï]/g,"i");
    str = str.replace(/[ÓÒÖÔ]/g,"O");
    str = str.replace(/[óòöô]/g,"o");
    str = str.replace(/[ÚÙÜÛ]/g,"U");
    str = str.replace(/[úùüû]/g,"u");
    //.... all the rest
    //str = str.replace(/[^a-z0-9]/gi,''); // final clean up
    return str; // final clean up
}

function fuzzyMatcher(item, query, fuzziness, pinStart) {
    fuzziness = regexFuzziness(fuzziness);
    pinStart = regexPinStart(pinStart);

    //Elimina caracteres especiales y reemplaza caracteres acentuados
    item = cleanUpSpecialChars(item);
    query = cleanUpSpecialChars(query);

    var pattern = query.split("").join("." + fuzziness);
    var regex = new RegExp(pinStart + pattern, "img");
    //console.log(pinStart + pattern);
    return regex.test(item);
}

function fuzzyHighlighter(item, query, joinHighlight, fuzziness, pinStart) {
    fuzziness = regexFuzziness(fuzziness);
    pinStart = regexPinStart(pinStart);

    //Elimina caracteres especiales y reemplaza caracteres acentuados
    //item = cleanUpSpecialChars(item);
    //query = cleanUpSpecialChars(query);

    var pattern = query.split("");
    var replace = '$1';
    if (joinHighlight === true) {
        replace = replace + "<b>$2</b>$3";
        pattern = "(" + pattern.join("." + fuzziness) + ")(.*)";
    } else {
        for (var i = 0; i < pattern.length; i++) {
            var j = (i * 2) + 1;
            replace = replace + "<b>" + "$" + (j + 1) + "</b>" + "$" + (j + 2);
        }
        pattern = "(" + pattern.join(")(." + fuzziness + ")(") + ")(.*)";
    }
    var regex = new RegExp(pinStart + pattern, "img");
    //console.log(pinStart + pattern, replace);
    return item.replace(regex, replace);
}

function regexFuzziness(fuzziness) {
    if (isNaN(fuzziness) || !fuzziness) {
        fuzziness = "*?";
    } else {
        fuzziness = "{0," + fuzziness + "}";
    }
    return fuzziness;
}

function regexPinStart(pinStart) {
    if (pinStart !== false) {
        return "(^)";
    }
    return "(.*?)";
}
